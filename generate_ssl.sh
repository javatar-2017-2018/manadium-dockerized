#!/bin/bash

ARG1=$1
prod=keys/production
dev=keys/development

test -d ${prod} || mkdir -p ${prod}
test -d ${dev} || mkdir -p ${dev}
test -d keys/$ARG1/server.key || echo Generating $ARG1 SSL keys... \
	&& openssl req -x509 -nodes -days 365 -newkey rsa:2048 -sha256 -out keys/${ARG1}/server.crt -keyout keys/$ARG1/server.key -passout pass:foobar

