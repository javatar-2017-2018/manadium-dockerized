PHONY: install help submodules production development
.DEFAULT_GOAL= help

help: ## Show help
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-10s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

init: ## Init and update submodules
	git submodule update --init --recursive

submodules: ## Init and update submodules
	git submodule update --recursive

production: ## Déploie le serveur de production
	bash ./generate_ssl.sh production
	bash ./deploy_ssl.sh production
	$(MAKE) -C web install
	docker-compose down
	docker-compose build
	docker-compose up -d
	sleep 10
	docker exec -ti manadiumdockerized_php-backend_1 php artisan down --message="Upgrading Database" --retry=60
	docker exec -ti manadiumdockerized_php-backend_1 make env-prod
	docker exec -ti manadiumdockerized_php-backend_1 make migrate
	docker exec -ti manadiumdockerized_php-backend_1 make link
	docker exec -ti manadiumdockerized_php-backend_1 php artisan up
	docker exec -ti manadiumdockerized_php-backend_1 chown www-data:www-data -R /var/www

development: ## Déploie le serveur de production
	bash ./generate_ssl.sh development
	bash ./deploy_ssl.sh development
	$(MAKE) -C web install-dev
	docker-compose down
	docker-compose build
	docker-compose up -d
	sleep 10
	docker exec -ti manadiumdockerized_php-backend_1 make env-dev
	docker exec -ti manadiumdockerized_php-backend_1 make migrate
	docker exec -ti manadiumdockerized_php-backend_1 make seed
	docker exec -ti manadiumdockerized_php-backend_1 make link
	docker exec -ti manadiumdockerized_php-backend_1 chown www-data:www-data -R /var/www

